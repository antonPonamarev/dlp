import { Injectable } from '@angular/core';
import { User } from '../classes/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/internal/operators/catchError';
import { EventDlp } from '../classes/event-dlp';
import { Departament } from '../classes/departament';

@Injectable({
  providedIn: 'root'
})
export class DlpService {

  constructor(private http: HttpClient) { }


  public usersFinder:any = {};

  public getUserById(idUser:string):User{
   //   if(this.usersFinder == null){
    //    this.getUsers().subscribe(r=>{
   //       return this.usersFinder[idUser]
    //    })
//}
    //  else{
        return this.usersFinder[idUser]
      //}
  }

  public getUsers():Observable<User[]>{
    return this.http
    .get<User[]>(`/api/user/list/`)
    .pipe(
      map(data => data.map(
        obj => {
          let user:User =  Object.assign(
            new User(), obj
            );
         this.usersFinder[obj["idUser"]] = user
          return user;
        }
      )),
      catchError(error => {
        throw error;
      })
    );
  }

  public getEvents():Observable<EventDlp[]>{
    return this.http
      .get<EventDlp[]>(`/api/event/get/`)
      .pipe(
        map(data => data.map(
          obj => Object.assign(
            new EventDlp(), obj
            )
        )),
        catchError(error => {
          throw error;
        })
      );
  }


  public getDepartaments():Observable<Departament[]>{
    return this.http
      .get<Departament[]>(`/api/user/listDepart/`)
      .pipe(
        map(data => data.map(
          obj => Object.assign(
            new Departament(), obj
            )
        )),
        catchError(error => {
          throw error;
        })
      );
  }
}
