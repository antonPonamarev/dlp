import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {GradientConfig} from '../../../../app-config';
import { NavigationItem } from './navigation';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  public navigation: any;
  constructor(public nav: NavigationItem) {
    this.navigation = this.nav.get();
  }

  ngOnInit() { }


}
