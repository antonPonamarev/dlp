import { Component, OnInit } from '@angular/core';
import { Departament } from 'src/app/classes/departament';
import { DlpService } from 'src/app/services/dlp.service';

@Component({
  selector: 'app-tbl-basic',
  templateUrl: './tbl-basic.component.html',
  styleUrls: ['./tbl-basic.component.scss']
})
export class TblBasicComponent implements OnInit {

  departaments:Departament[]
  constructor() {
      this.departaments = [
        {
          NameDepart: "Логистика",
          idDepart:1
        },
        {
          NameDepart: "Разработка",
          idDepart: 2
        }
      ]
   }



  ngOnInit() {

  }

}
