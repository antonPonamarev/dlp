import { Pipe, PipeTransform } from '@angular/core';
import { EventDlp } from './event-dlp';

@Pipe({
    name: 'eventfilter',
    pure: false
})
export class EventFilter implements PipeTransform {
    transform(items: EventDlp[], filter: Object): any {
        if (!items || !filter || filter["type"] === undefined) {
            return items;
        }
        // filter items array, items which match and return true will be
        // kept, false will be filtered out
        return items.filter(r => r.type == filter["type"]);
    }
}