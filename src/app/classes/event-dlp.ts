import { User } from "./user"

export class EventDlp {
    user_id:number = -1
    _action:string = ""
    hash:string = ""
    date:string = ""
    detail:string = ""
    idMsg:string = ""
    second_user_id:string = ""
    
    eventType:any = {
        "create_user" : {
            title: "Создан пользователь",
            type : "user"
        },
        "warning_key" : {
            title : "Опасное сочетание в чате",
            type : "chat"
        },
        "try_login" : {
            title : "Попытка входа",
            type : "login"
        },
        "dialog_start" : {
            title : "Диалог начат",
            type : "dialog_start"
        },
        "file_send_dialog" : {
            title: "Отправка фото в диалоге",
            type : "file_send_dialog"
        }
    }


    public set action(value:string){
        this._action = value
    }

    public get action():string{
        return this.eventType[this._action] !== undefined ? this.eventType[this._action].title : this._action;
    }

    public get type():string{
        return this.eventType[this._action] !== undefined ? this.eventType[this._action].type : "-";
    }

}
