import { Component, OnInit } from '@angular/core';
import { Departament } from 'src/app/classes/departament';
import { EventDlp } from 'src/app/classes/event-dlp';
import { User } from 'src/app/classes/user';
import { DlpService } from 'src/app/services/dlp.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  departaments:Departament[]
  users:User[]

  filterType:any[] = []

  currentFilter:any = {
    //type : "login"
  }

  events:EventDlp[]
  constructor(private ds: DlpService) {
      this.ds.getEvents().subscribe(r=>{
        this.events = r

        this.events.forEach(e => {
          if( this.filterType.indexOf(e.type) == -1 ){
              this.filterType.push(e.type)
          }
        })

      })
      this.ds.getUsers().subscribe(r=>{

      })
   }


  ngOnInit(): void {
  }

  public getUser(idUser:string){
    return this.ds.usersFinder[idUser]
  }

  public getObj(type:string,e:EventDlp ){
    switch(type){
      case "-":{
        return "-"
        break;
      }
        case "user" :{
          return this.getUser(e.detail).fio
        }
        case "login" :{
          return e.detail
        }
        case "dialog_start" :{
          return `${this.getUser(e.detail).fio} [${e.detail}]`
        }
        case "file_send_dialog" :{
          return `${this.getUser(e.second_user_id).fio} [${e.second_user_id}]`
        }
    }
  }

}
