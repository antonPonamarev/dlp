import { Component, OnInit } from '@angular/core';
import { Departament } from 'src/app/classes/departament';
import { User } from 'src/app/classes/user';
import { DlpService } from 'src/app/services/dlp.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  departaments:Departament[]
  users:User[]

  constructor(private ds: DlpService) {

    ds.getDepartaments().subscribe((r) => {
      this.departaments = r

      ds.getUsers().subscribe(r=>{
        this.users = r
      })
    })
   }


  ngOnInit(): void {
  }

}
