import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { EventsComponent } from '../events/events.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { DlpModuleRouterModule } from './dlp-module-router.module';
import { CardModule } from 'src/app/theme/shared/components/card/card.module';
import { UsersComponent } from '../users/users.component';
import { IncidentComponent } from '../incident/incident.component';
import { EventFilter } from 'src/app/classes/event-filter';


const routes: Routes = [
  {
    path: 'events',
    component: EventsComponent
  }
];

@NgModule({
  declarations: [
    EventsComponent,
    UsersComponent,
    IncidentComponent,
    EventFilter
  ],
  imports: [
    CommonModule,
    DlpModuleRouterModule,
    CardModule,
  ]
})
export class DlpModuleModule { }
