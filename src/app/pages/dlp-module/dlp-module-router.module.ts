import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventsComponent } from '../events/events.component';
import { IncidentComponent } from '../incident/incident.component';
import { UsersComponent } from '../users/users.component';

const routes: Routes = [
  {
    path: 'events',
    component: EventsComponent
  },
  {
    path: 'users',
    component: UsersComponent
  },
  {
    path: 'incident',
    component: IncidentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DlpModuleRouterModule { }
